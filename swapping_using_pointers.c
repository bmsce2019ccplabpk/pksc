#include<stdio.h>
void swap(int*x,int*y);
int main( )
{
    int x,y;
    printf("Enter value of x:\n");
    scanf("%d",&x);
    printf("Enter value of y:\n");
    scanf("%d",&y);
    printf("Before Swapping: x is:%d, y is:%d\n",x,y);
    swap(&x,&y);
    printf("After swapping: x is:%d,y is:%d\n",x,y);
    return 0;
}
void swap(int *x,int *y)
{
    int t;
    t = *x;
    *x = *y;
    *y =  t;
}