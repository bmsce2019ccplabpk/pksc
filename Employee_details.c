#include<stdio.h>
int main ( )
{
    struct employee
    {
        char name[25];
        int empId;
        float salary;
        char DOJ[10];
    };
    struct employee emp;
    printf("Enter the name,ID,salary,DOJ of the employee\n");
    scanf("%s%d%f%s",&emp.name,&emp.empId,&emp.salary,&emp.DOJ);
    printf("employee details : \n");
    printf("name of the employee is %s\n",emp.name);
    printf("empID of the employee is %d\n",emp.empId);
    printf("salary of the employee is %f\n",emp.salary);
    printf("date of joining of the employee id %s\n",emp.DOJ);
    return 0;
}